INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2024, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 16.02.2024 | 1.0     | OPENGIS.ch          | Ersterstellung
!! 07.03.2024 | 1.0     | eb                  | Bearbeitung eb gem. Sprint 03/2024
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_afk_augusta_raurica_rohstoffabbau_v1_0 (de)
AT "http://models.geo.bl.ch/AFK/"
VERSION "2024-02-16" =

  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0;
  IMPORTS CatalogueObjects_V1;

  TOPIC codelisten =
    CLASS rohstoffabbau_typ_item
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*10;
      Text:  MANDATORY TEXT*200;
    END rohstoffabbau_typ_item;

    STRUCTURE rohstoffabbau_typ_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) rohstoffabbau_typ_item;
    END rohstoffabbau_typ_ref;
  END codelisten;

  TOPIC ch_bl_afk_augusta_raurica_rohstoffabbau =

    DEPENDS ON ch_bl_afk_augusta_raurica_rohstoffabbau_v1_0.codelisten;

    CLASS rohstoffabbau_flaeche =
      vorgangsnr : TEXT*10;
      typ : ch_bl_afk_augusta_raurica_rohstoffabbau_v1_0.codelisten.rohstoffabbau_typ_ref;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLMultiFlaeche2D;
    END rohstoffabbau_flaeche;

    CLASS rohstoffabbau_linie =
      vorgangsnr : TEXT*10;
      typ : ch_bl_afk_augusta_raurica_rohstoffabbau_v1_0.codelisten.rohstoffabbau_typ_ref;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLMultiLinie2D;
    END rohstoffabbau_linie;

  END ch_bl_afk_augusta_raurica_rohstoffabbau;

END ch_bl_afk_augusta_raurica_rohstoffabbau_v1_0.
    

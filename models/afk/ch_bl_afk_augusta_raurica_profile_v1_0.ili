INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2024, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 16.02.2024 | 1.0     | OPENGIS.ch          | Ersterstellung
!! 07.03.2024 | 1.0     | eb                  | Bearbeitung eb gem. Sprint 03/2024
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_afk_augusta_raurica_profile_v1_0 (de)
AT "http://models.geo.bl.ch/AFK/"
VERSION "2024-02-16" =
  IMPORTS CatalogueObjects_V1;
  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0;
  IMPORTS ch_bl_afk_augusta_raurica_kataloge_v1_0;

  TOPIC codelisten =
    CLASS blick_nach_item
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*10;
      Text:  MANDATORY TEXT*50;
    END blick_nach_item;

    STRUCTURE blick_nach_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) blick_nach_item;
    END blick_nach_ref;
  END codelisten;

  TOPIC ch_bl_afk_augusta_raurica_profile =
    DEPENDS ON ch_bl_afk_augusta_raurica_profile_v1_0.codelisten;

    CLASS profil_linie =
      blick_nach : ch_bl_afk_augusta_raurica_profile_v1_0.codelisten.blick_nach_ref;
      vorgangsnr : TEXT*10;
      profilnr : TEXT*50;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLMultiLinie2D;
    END profil_linie;

    CLASS profil_punkt =
      angle : 0.00 .. 359.99;
      textstring : MTEXT*50;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLKoord2D;
    END profil_punkt;

  END ch_bl_afk_augusta_raurica_profile;

END ch_bl_afk_augusta_raurica_profile_v1_0.
    

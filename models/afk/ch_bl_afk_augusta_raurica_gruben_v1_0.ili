INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2024, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 16.02.2024 | 1.0     | OPENGIS.ch          | Ersterstellung
!! 07.03.2024 | 1.0     | eb                  | Bearbeitung eb gem. Sprint 03/2024
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_afk_augusta_raurica_gruben_v1_0 (de)
AT "http://models.geo.bl.ch/AFK/"
VERSION "2024-02-16" =

  IMPORTS CatalogueObjects_V1;
  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0;
  IMPORTS ch_bl_afk_augusta_raurica_kataloge_v1_0;

  TOPIC codelisten =
    CLASS grube_typ_item
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*10;
      Text:  MANDATORY TEXT*200;
    END grube_typ_item;

    STRUCTURE grube_typ_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) grube_typ_item;
    END grube_typ_ref;
  END codelisten;

  TOPIC ch_bl_afk_augusta_raurica_gruben =
    DEPENDS ON ch_bl_afk_augusta_raurica_gruben_v1_0.codelisten;
    DEPENDS ON ch_bl_afk_augusta_raurica_kataloge_v1_0.codelisten;

    CLASS grube_flaeche =
      vorgangsnr : TEXT*10;
      art_grube : ch_bl_afk_augusta_raurica_gruben_v1_0.codelisten.grube_typ_ref;
      epoche : ch_bl_afk_augusta_raurica_kataloge_v1_0.codelisten.epoche_ref;
      herkunft : ch_bl_afk_augusta_raurica_kataloge_v1_0.codelisten.herkunft_ref;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLMultiFlaeche2D;
    END grube_flaeche;

    CLASS grube_linie =
      vorgangsnr : TEXT*10;
      art_grube : ch_bl_afk_augusta_raurica_gruben_v1_0.codelisten.grube_typ_ref;
      epoche : ch_bl_afk_augusta_raurica_kataloge_v1_0.codelisten.epoche_ref;
      herkunft : ch_bl_afk_augusta_raurica_kataloge_v1_0.codelisten.herkunft_ref;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLMultiLinie2D;
    END grube_linie;

  END ch_bl_afk_augusta_raurica_gruben;

END ch_bl_afk_augusta_raurica_gruben_v1_0.
    

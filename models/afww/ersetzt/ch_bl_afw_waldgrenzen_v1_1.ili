INTERLIS 2.3;
!!=========================================================================================================================================
!! Volkswirtschafts- und Gesundheitsdirektion 
!! Amt für Geoinformation
!! GIS-Fachstelle
!! Mühlemattstrasse 36
!! 4410 Liestal

!! www.geo.bl.ch
!! 
!! Kantonales Geodatenmodell Statische Waldgrenzen gemäss Geobasisdatensatz Nr. 157
!!=========================================================================================================================================
!! Revision History
!!
!! 02.08.2017 / um erstellt
!! 03.08.2017 / um Anpassung aufgrund Besprechung mit Bu
!! 11.08.2017 / um Anpassung aufgrund Besprechung mit Bu
!! 14.08.2017 / um Rückmeldungen Ro eingearbeitet
!! 05.09.2017 / um Anpassung Model aufgrund neuer Kenntnisse betr. Rodung
!! 29.09.2017 / um Anpassung Model aufgrund neuer Kenntnisse betr. Rodung (Besprechung 27.09.2017)
!! 23.11.2017 / um Anpassung Model nach Stellungnahme Geometerbüros
!! 06.07.2022 / um Anpassung Model Name von Waldgrenzen_BL_V1_LV95 zu ch_bl_afw_waldgrenzen_v1_1, 
!!				   CLASS Waldgrenze: neues Attribut Art_laufendeAenderung inkl. DOMAIN Werte
!!				   CLASS Waldgrenze: Ergaenzung CONSTRAINT				   				    			  
!!=========================================================================================================================================
!!@ origin=UML/INTERLIS-Editor
!!@ IDGeoIV=157
!!@ author ="Ursula Monzeglio"
!!@ technicalContact=mailto:support.gis@bl.ch 
!!@ furtherInformation=http://www.wald-basel.ch/
!!=========================================================================================================================================

MODEL ch_bl_afw_waldgrenzen_v1_1 (de)
AT "http://models.geo.bl.ch/AGI"
VERSION "2022-07-06"  =

  IMPORTS CHAdminCodes_V1,GeometryCHLV95_V1,InternationalCodes_V1;

  DOMAIN

  Art_Waldgrenze = (
    in_Bauzonen,
    ausserhalb_Bauzonen
  );
	
  Art_laufendeAenderung = (
    neu,
    aufgehoben
  );

  Rechtsstatus = (
    inKraft,
    laufendeAenderungen
  );

  Verbindlichkeit = (
    Nutzungsplanfestlegung,
    orientierend
  );

  TOPIC Geobasisdaten =

    CLASS Waldgrenze =
      Art : MANDATORY ch_bl_afw_waldgrenzen_v1_1.Art_Waldgrenze;
      Art_laufendeAenderung : ch_bl_afw_waldgrenzen_v1_1.Art_laufendeAenderung;
      Verbindlichkeit : MANDATORY ch_bl_afw_waldgrenzen_v1_1.Verbindlichkeit;
      Rechtsstatus : MANDATORY ch_bl_afw_waldgrenzen_v1_1.Rechtsstatus;
      publiziertAb : INTERLIS.XMLDate;
      LexLink : 1 .. 2147483647;
	  
      !! Wenn der Rechtsstatus «laufendeAenderungen» ist, muss die Art der laufenden Aenderung angegeben werden.
	  !! Wenn der Rechtsstatus «inKraft» ist die Art der laufenden Aenderung leer.
      MANDATORY CONSTRAINT 
        ((Rechtsstatus == #laufendeAenderungen ) AND (DEFINED (Art_laufendeAenderung)))
        OR ((Rechtsstatus == #inKraft) AND NOT (DEFINED (Art_laufendeAenderung)));
      
    END Waldgrenze;

    CLASS Waldgrenze_Linie =
      Geometrie : MANDATORY GeometryCHLV95_V1.Line;
    END Waldgrenze_Linie;

    ASSOCIATION Waldgrenze_Geometrie =
      WG -<#> {1} Waldgrenze;
      Geometrie -- {1..*} Waldgrenze_Linie;
    END Waldgrenze_Geometrie;

    CLASS Waldgrenzenkarte =
      Plan_Nr : MANDATORY TEXT*40;
      Plan_Name : MANDATORY TEXT*100;
      Perimeter : GeometryCHLV95_V1.Surface;
    END Waldgrenzenkarte;

    ASSOCIATION Waldgrenze_Waldgrenzenkarte =
      WGK -- {0..*} Waldgrenzenkarte;
      WG -<> {1} Waldgrenze;
    END Waldgrenze_Waldgrenzenkarte;

  END Geobasisdaten;

  TOPIC TransferMetadaten =

    CLASS Amt =
      Name : MANDATORY TEXT*80;
      AmtImWeb : URI;
      UID : TEXT*12;
      Datenbearbeitungsstelle : MANDATORY TEXT*80;
    END Amt;

    CLASS Datenbestand =
      Gemeinde_ID_BFS : MANDATORY 2761 .. 2895;
      Stand : MANDATORY INTERLIS.XMLDate;
      Bemerkungen : MTEXT;
    END Datenbestand;

  END TransferMetadaten;

END ch_bl_afw_waldgrenzen_v1_1.

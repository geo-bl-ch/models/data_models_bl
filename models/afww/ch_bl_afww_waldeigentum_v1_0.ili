INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2021, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in                     | Aenderung
!!----------------------------------------------------------------------------------------
!! 20.07.2021 | 1.0     | Jasmin Wenger, Ruth Albrecht | Ersterstellung
!! 27.10.2022 | 1.0     | K. Deininger                 | Verwende bl_basis_kgdm_lv95_v2_0
!! 29.08.2024 | 1.0     | Roman Meyer                  | Umbenennung Waldkarte zu Waldeigentum
!!----------------------------------------------------------------------------------------
!!@ technicalContact=mailto:support.gis@bl.ch
!!@ furtherInformation=https://geo.bl.ch
!!========================================================================================

MODEL ch_bl_afww_waldeigentum_codelisten_v1_0 (de) AT "http://models.geo.bl.ch/afww/" VERSION "2024-08-29" =

  IMPORTS CatalogueObjects_V1;

  TOPIC Codelisten =

    CLASS eigentuemer_catalogue
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*40;
      Text:  MANDATORY TEXT*40;
    END eigentuemer_catalogue;

    STRUCTURE eigentuemer_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) eigentuemer_catalogue;
    END eigentuemer_ref;

    CLASS einrichtung_catalogue
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*40;
      Text:  MANDATORY TEXT*40;
    END einrichtung_catalogue;

    STRUCTURE einrichtung_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) einrichtung_catalogue;
    END einrichtung_ref;

  END Codelisten;

END ch_bl_afww_waldeigentum_codelisten_v1_0.


MODEL ch_bl_afww_waldeigentum_v1_0 (de) AT "http://models.geo.bl.ch/afww/" VERSION "2024-08-29" =

  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS, CatalogueObjects_V1;
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0, ch_bl_afww_waldeigentum_codelisten_v1_0;

  TOPIC waldeigentum =
    DEPENDS ON ch_bl_afww_waldeigentum_codelisten_v1_0.Codelisten;

    CLASS waldeigentum =
      id:                 MANDATORY 1 .. 2147483647;
      gemeindename:       MANDATORY TEXT*30;
      eigentuemer:        MANDATORY ch_bl_afww_waldeigentum_codelisten_v1_0.Codelisten.eigentuemer_ref;
      einrichtung:        MANDATORY ch_bl_afww_waldeigentum_codelisten_v1_0.Codelisten.einrichtung_ref;
      flaeche:            MANDATORY 0.00 .. 99999.99 [a];
      geometrie:          MANDATORY BLMultiFlaeche2D;
      UNIQUE id;
    END waldeigentum;

  END waldeigentum;

END ch_bl_afww_waldeigentum_v1_0.

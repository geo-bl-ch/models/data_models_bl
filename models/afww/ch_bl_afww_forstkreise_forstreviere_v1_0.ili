INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2021, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 26.04.2021 | 1.0     | um                  | Ersterstellung
!! 01.07.2021 | 1.0     | um                  | Anpassung Geometrie von BLKoord2D (Pkt)
!!            |         |                     | auf BLMultiFlaeche2D (Flaechen)
!! 27.10.2022 | 1.0     | K. Deininger        | Verwende bl_basis_kgdm_lv95_v2_0
!!========================================================================================

!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
MODEL ch_bl_afww_forstkreise_forstreviere_v1_0 (de) AT "http://models.geo.bl.ch/afww/" VERSION "2022-10-27" =

  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0;

  TOPIC forstkreise_forstreviere =

    CLASS forstkreis =
      id:                 MANDATORY 1 .. 2147483647;
      forstkreis:         MANDATORY TEXT*255;
      geometrie:          MANDATORY BLMultiFlaeche2D;
      UNIQUE id;
    END forstkreis;

    CLASS forstrevier =
      id:                 MANDATORY 1 .. 2147483647;
      forstrevier:        MANDATORY TEXT*255;
      nachname:           MANDATORY TEXT*255;
      vorname:            MANDATORY TEXT*255;
      strasse:            MANDATORY TEXT*255;
      plz:                MANDATORY TEXT*255;
      ort:                MANDATORY TEXT*255;
      waldflaeche:        MANDATORY 0.0 .. 99999.9 [Units.ha];
      geometrie:          MANDATORY BLMultiFlaeche2D;
      UNIQUE id;
    END forstrevier;

    ASSOCIATION ForstrevierForstkreis =
      Kreis -- {1} forstkreis;
      Revier -<> {1..*} forstrevier;
    END ForstrevierForstkreis;

  END forstkreise_forstreviere;

END ch_bl_afww_forstkreise_forstreviere_v1_0.

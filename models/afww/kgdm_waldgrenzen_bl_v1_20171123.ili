INTERLIS 2.3;
!!=========================================================================================================================================
!! Kanton Basel-Landschaft
!! Volkswirtschafts- und Gesundheitsdirektion 
!! Amt für Geoinformation
!! GIS-Fachstelle
!! Mühlemattstrasse 36
!! 4410 Liestal

!! www.geo.bl.ch
!! 
!! Kantonales Geodatenmodell Statische Waldgrenzen gemäss Geobasisdatensatz Nr. 157
!!=========================================================================================================================================
!! Revision History
!!
!! 02.08.2017 / um erstellt
!! 03.08.2017 / um Anpassung aufgrund Besprechung mit Bu
!! 11.08.2017 / um Anpassung aufgrund Besprechung mit Bu
!! 14.08.2017 / um Rückmeldungen Ro eingearbeitet
!! 05.09.2017 / um Anpassung Model aufgrund neuer Kenntnisse betr. Rodung
!! 29.09.2017 / um Anpassung Model aufgrund neuer Kenntnisse betr. Rodung (Besprechung 27.09.2017)
!! 23.11.2017 / um Anpassung Model nach Stellungnahme Geometerbüros
!!=========================================================================================================================================
!!@ origin=UML/INTERLIS-Editor
!!@ furtherInformation=http://www.agi.bl.ch
!!@ IDGeoIV=157
!!@ author ="Ursula Monzeglio"
!!@ technicalContact=mailto:support.gis@bl.ch 
!!@ furtherInformation=http://www.agi.bl.ch
!!=========================================================================================================================================

MODEL Waldgrenzen_BL_V1_LV95 (de)
AT "http://models.geo.bl.ch/AfW"
VERSION "2017-11-23"  =
  IMPORTS CHAdminCodes_V1,GeometryCHLV95_V1,InternationalCodes_V1;

 DOMAIN  
    Art_Waldgrenze = (
      in_Bauzonen,
      ausserhalb_Bauzonen
    );

    Rechtsstatus = (
      inKraft,
      laufendeAenderungen
    );

    Verbindlichkeit = (
      Nutzungsplanfestlegung,
      orientierend
    );

  TOPIC Geobasisdaten =
   
    CLASS Waldgrenze =
      Art : MANDATORY Waldgrenzen_BL_V1_LV95.Art_Waldgrenze;
      Verbindlichkeit : MANDATORY Waldgrenzen_BL_V1_LV95.Verbindlichkeit;
      Rechtsstatus : MANDATORY Waldgrenzen_BL_V1_LV95.Rechtsstatus;
      publiziertAb : INTERLIS.XMLDate; !!Genehmigungsdatum (in Kraft Datum)!!
	  LexLink : 1 .. 2147483647;
    END Waldgrenze;

    CLASS Waldgrenze_Linie =
      Geometrie : MANDATORY GeometryCHLV95_V1.Line;
    END Waldgrenze_Linie;
	
    ASSOCIATION Waldgrenze_Geometrie =
      WG -<#> {1} Waldgrenze;
      Geometrie -- {1..*} Waldgrenze_Linie;
    END Waldgrenze_Geometrie;
	
	CLASS Waldgrenzenkarte =
      Plan_Nr : MANDATORY TEXT*40;	!!Inventarnummer!!
      Plan_Name : MANDATORY TEXT*100; !!Planname!!
      Perimeter : GeometryCHLV95_V1.Surface;
    END Waldgrenzenkarte;
	
    ASSOCIATION Waldgrenze_Waldgrenzenkarte =
      WGK -- {0..*} Waldgrenzenkarte;
      WG -<> {1} Waldgrenze;
    END Waldgrenze_Waldgrenzenkarte;

  END Geobasisdaten;

  TOPIC TransferMetadaten =

    CLASS Amt =
      Name : MANDATORY TEXT*80; !!Amt für Wald beider Basel!!
      AmtImWeb : URI;			!!http://www.wald-basel.ch!!
	  UID: TEXT*12;				!!leer lassen! 
      Datenbearbeitungsstelle : MANDATORY TEXT*80;
    END Amt;

    CLASS Datenbestand =
      Gemeinde_ID_BFS : MANDATORY 2761 .. 2895;
      Stand : MANDATORY INTERLIS.XMLDate;
      Bemerkungen : MTEXT;
    END Datenbestand;

  END TransferMetadaten;

END Waldgrenzen_BL_V1_LV95.

 TRANSFER GHKBL25;

 MODEL GEFAHRENHINWEISKARTE_BL

 DOMAIN
   LKoord = COORD2 480000.000  70000.000 
                   840000.000 300000.000;

   I4 = [1 .. 9999];
   I8 = [1 .. 99999999];
   I9 = [1 .. 999999999];
   FLOAT = [1.0 .. 10.0];

   T255 = TEXT*255;


   Ufererosion = (Ufererosion_moeglich, Ufererosion_teilw_moeglich, Ufererosion_unwahrscheinlich);
   WAusbruch = (Durchlass_oder_Bruecke, Dolung, reliefbedingt);
   Kornverteilung = (nicht_relevant, tonig_siltig, sandig_kiesig, grob_kiesig_blockig);
   DRelevanz = (nicht_relevant, gering, mittel, gross);
   Schuttprod = (kein_Fels, gering, mittel, gross);
   SProzess = (Transit_Ablagerung, Ausbruchzonen_Schuttwald, Ausbruchzonen_Fels);
   Reibungswinkelklasse = (nicht_definiert, bis25Grad, zwischen25und30Grad, zwischen30und35Grad, groesser35Grad, zwischen15und20GradfuerModellierung2);
 



TOPIC GHK = 

      TABLE P_AUT = 
            !! Autoren der Geodaten
            P_AUT_ID:  I9;
            P_AUT_BEZ: T255;
      IDENT
            P_AUT_ID;
      END P_AUT;  


      TABLE P_PROJ = 
            !! Bezeichnung des Projekts
            P_PROJ_ID:  I9;  
            P_PROJ_BEZ: T255;
      IDENT
            P_PROJ_ID;
      END P_PROJ;  


      TABLE P_SRC = 
            !! Liste der Verfahren und Grundlagendaten
	    P_SRC_ID:  I9;  
            P_SRC_BEZ: T255;
            P_SRC_JAHR: I4;
      IDENT
            P_SRC_ID;
      END P_SRC;  


      TABLE P_SRC_LINK = 
            !! Verknuepfung der Verfahren mit den Grundlagendaten 
	    P_SRC_LINK_ID:  I9;
	    P_SRC_ID1:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
	    P_SRC_ID2:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
      IDENT
            P_SRC_LINK_ID;
      END P_SRC_LINK;  

  


      TABLE GH_RPE = 
            !! Gefahrenhinweis Permanente Rutschungen im Lockergestein, erwiesen
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_RPE_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_RPE_ID;
      END GH_RPE;  


      TABLE GH_RS = 
            !! Gefahrenhinweis Spontane Rutschungen im Lockergestein
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_RS_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_RS_ID;
      END GH_RS;  


      TABLE GH_RPP = 
            !! Gefahrenhinweis Permanente Rutschungen im Lockergestein, potentiell
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_RPP_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_RPP_ID;
      END GH_RPP;   

      TABLE GH_SAE = 
            !! Gefahrenhinweis Sackungen, erwiesen
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_SAE_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_SAE_ID;
      END GH_SAE;     

      TABLE GH_RFP = 
            !! Gefahrenhinweis Felsrutschung, potentiell
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_RFP_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_RFP_ID;
      END GH_RFP;       

      TABLE GH_ASE = 
            !! Gefahrenhinweis Abflusslose Senke, erwiesen
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_ASE_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_ASE_ID;
      END GH_ASE;         

      TABLE GH_DOP = 
            !! Gefahrenhinweis Doline, potentiell
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_DOP_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_DOP_ID;
      END GH_DOP;            

      TABLE GH_DOE = 
            !! Gefahrenhinweis Doline, erwiesen
	    SHAPE :	LKoord;
            GH_DOE_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_DOE_ID;
      END GH_DOE;               

      TABLE GH_S = 
            !! Gefahrenhinweis Stein- und Blockschlag
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_S_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
            CODE_PROZ_S: SProzess;
      IDENT
            GH_S_ID;
      END GH_S;              

      TABLE GH_MG = 
            !! Gefahrenhinweis Murgang
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_MG_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_MG_ID;
      END GH_MG;                  

      TABLE GH_UFS = 
            !! Gefahrenhinweis Überflutung aus steilen Gerinnen
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_UFS_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_UFS_ID;
      END GH_UFS;                     

      TABLE GH_UFF = 
            !! Gefahrenhinweis Überflutung aus flachen Gerinnen
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_UFF_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_UFF_ID;
      END GH_UFF;                        

      TABLE GH_USS = 
            !! Gefahrenhinweis Übersarung aus steilen Gerinnen
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            GH_USS_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            GH_USS_ID;
      END GH_USS;                        

      TABLE GH_UES = 
            !! Gefahrenhinweis Ufererosion in steilen Gerinnen
	    SHAPE :	POLYLINE WITH (STRAIGHTS) VERTEX LKoord;
            GH_UES_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
            CODE_UFER: Ufererosion;
      IDENT
            GH_UES_ID;
      END GH_UES;                          

      TABLE GH_UEF = 
            !! Gefahrenhinweis Ufererosion in flachen Gerinnen
	    SHAPE :	POLYLINE WITH (STRAIGHTS) VERTEX LKoord;
            GH_UEF_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
            CODE_UFER: Ufererosion;
      IDENT
            GH_UEF_ID;
      END GH_UEF;                 
                   

      TABLE Z_WAS = 
            !! Zwischenprodukt: Ausbruchstellen in steilen Gerinnen
	    SHAPE :	LKoord;
            Z_WAS_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
            CODE_AUSB_W: WAusbruch;
      IDENT
            Z_WAS_ID;
      END Z_WAS;                     
                   

      TABLE Z_G = 
            !! Zwischenprodukt: Geologie (Matrix, Durchlässigkeit, Schuttproduktion)
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            Z_G_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
            CODE_KORN: Kornverteilung;
            CODE_D_REL: DRelevanz;
            CODE_SPROD: Schuttprod;
      IDENT
            Z_G_ID;
      END Z_G;                         
                   

      TABLE Z_GRW = 
            !! Zwischenprodukt: Geologie (Reibungswinkelklasse)
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            Z_GRW_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
            CODE_GRW: Reibungswinkelklasse;
      IDENT
            Z_GRW_ID;
      END Z_GRW;                         
                   

      TABLE Z_SAS = 
            !! Zwischenprodukt: Steinschlag Ausbruch Schuttwald
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            Z_SAS_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            Z_SAS_ID;
      END Z_SAS;                          
                   

      TABLE Z_SAF = 
            !! Zwischenprodukt: Steinschlag Ausbruch Fels
	    SHAPE :	SURFACE WITH (STRAIGHTS) VERTEX LKoord;
            Z_SAF_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
      IDENT
            Z_SAF_ID;
      END Z_SAF;                           
                   

      TABLE Z_C100 = 
            !! Zwischenprodukt: Abflussbeiwerte C-100
	    SHAPE :	POLYLINE WITH (STRAIGHTS) VERTEX LKoord;
            Z_C100_ID:  I9;
            P_AUT_ID:  I9;  !!Fremdschlüssel zu Tabelle P_AUT
            P_PROJ_ID:  I9;  !!Fremdschlüssel zu Tabelle P_PROJ
            P_SRC_ID:  I9;  !!Fremdschlüssel zu Tabelle P_SRC
            IMSTB: I9;
            EDATUM: I8;
            C100: FLOAT;
      IDENT
            Z_C100_ID;
      END Z_C100;        





END GHK. !! Ende Topic



 END GEFAHRENHINWEISKARTE_BL.  !! Ende Model

 FORMAT FREE;
 CODE
   BLANK = DEFAULT, UNDEFINED = DEFAULT, CONTINUE = DEFAULT;
 TID = ANY;
 END.
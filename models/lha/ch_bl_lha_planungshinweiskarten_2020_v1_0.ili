INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2023, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 06.07.2023 | 1.0     | OPENGIS.ch          | Ersterstellung
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_lha_planungshinweiskarten_2020_v1_0 (de)
AT "http://models.geo.bl.ch/LHA"
VERSION "2023-07-06" =

  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0;

  TOPIC ch_bl_lha_planungshinweiskarten_2020 =
    
    CLASS gemeinde_mit_planungsrelevantem_klima =
      phk_n8_ist : BOOLEAN;
      phk_n8_zuk : BOOLEAN;
      phk_tg_ist : BOOLEAN;
      phk_tg_zuk : BOOLEAN;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
    END gemeinde_mit_planungsrelevantem_klima;

    CLASS tag_14uhr_bewertung_gruen_und_freiflaechen =
      bew_gf_tag : -5.000000 .. 5.000000;
      bewertung : TEXT*100;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
    END tag_14uhr_bewertung_gruen_und_freiflaechen;

    CLASS tag_14uhr_bioklimat_belastung_siedlung =
      phk_pet14 : -5.000000 .. 5.000000;
      bewertung : TEXT*100;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
    END tag_14uhr_bioklimat_belastung_siedlung;

    CLASS tag_14uhr_aufenthaltsqualitaet_strassen =
      pet14z_ist : -5.000000 .. 5.000000;
      bewertung : TEXT*100;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLLinie2D;
    END tag_14uhr_aufenthaltsqualitaet_strassen;

    CLASS nacht_4uhr_bewertung_gruen_und_freiflaechen =
      phk_n8_gf : -5.000000 .. 5.000000;
      bewertung : TEXT*100;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
    END nacht_4uhr_bewertung_gruen_und_freiflaechen;

    CLASS nacht_4uhr_bioklimat_belastung_siedlung =
      phk_t04 : -5.000000 .. 5.000000;
      bewertung : TEXT*100;
      geometrie : bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
    END nacht_4uhr_bioklimat_belastung_siedlung;

  END ch_bl_lha_planungshinweiskarten_2020;

END ch_bl_lha_planungshinweiskarten_2020_v1_0.

INTERLIS 2.3;

!!========================================================================================
!! Datum      | Version | Autor/in                     | Aenderung
!!----------------------------------------------------------------------------------------
!! 2021-03-12 | 1.0     | Karsten Deininger            | Erstfassung, Version 1.0
!! 2022-03-17 | 1.0     | Karsten Deininger            | Neues Attribut "gesperrt"
!! 2022-05-04 | 1.0     | Karsten Deininger            | Neue Attribute "lst_typ" und
!!            |         |                              | "rht_typ"
!! 2023-02-03 | 1.0     | Karsten Deininger            | Neues Attribut "unternehmen_id"
!!            |         |                              | (UID)
!!========================================================================================

!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch/"
MODEL ch_bl_zrv_eigentumsauskunft_v1_0 (de)
AT "https://models.geo.bl.ch/ZRV/"
VERSION "2023-02-03"  =
  IMPORTS UNQUALIFIED INTERLIS;

  TOPIC eigentumsauskunft =
    OID AS INTERLIS.UUIDOID;

    CLASS adresse =
      ase_id: MANDATORY 0 .. 9999999;
      ase_typ: TEXT*1;
      akt_vorname: TEXT*256;
      akt_name: TEXT*256;
      strasse: TEXT*100;
      postleitzahl: TEXT*25;
      ort: TEXT*40;
      land: TEXT*100;
      anrede: TEXT*3;
      titel: TEXT*2;
      unternehmen_id: TEXT*20;
      ase_rechtsform: TEXT*2;
      ase_rechtsform_txt: TEXT*100;
      verstorben: TEXT*8;
      gesperrt: 0 .. 1;
      loesch_status: TEXT*1;
    UNIQUE ase_id;
    END adresse;

    CLASS egris_egrid =
      gdk_id: MANDATORY TEXT*12;
      esta_nr: MANDATORY TEXT*4;
      gdk_art: MANDATORY TEXT*1;
      egrid: TEXT*22;
    UNIQUE gdk_id, esta_nr, gdk_art;
    END egris_egrid;

    CLASS gemeinde =
      esta_nr: MANDATORY TEXT*4;
      gem_name: TEXT*100;
      esta_b: TEXT*4;
      kanton: TEXT*30;
      ggem_name: TEXT*30;
    UNIQUE esta_nr;
    END gemeinde;

    CLASS mitglied =
      ase_id: MANDATORY 0 .. 9999999;
      mld_von: MANDATORY 0 .. 9999999;
      grund_datum: MANDATORY TEXT*8;
      grund_nr: MANDATORY TEXT*7;
      loesch_status: TEXT*1;
    UNIQUE ase_id, mld_von, grund_datum, grund_nr;
    END mitglied;

    CLASS recht =
      rht_id: MANDATORY TEXT*10;
      gdk_last: TEXT*12;
      esta_last: TEXT*4;
      art_last: TEXT*1;
      lst_typ: TEXT*1;
      gdk_recht: TEXT*12;
      esta_recht: TEXT*4;
      art_recht: TEXT*1;
      rht_typ: TEXT*1;
      ase_id: 0 .. 9999999;
      ase_typ: TEXT*1;
      ase_rechtsform: TEXT*2;
      ase_rechtsform_txt: TEXT*100;
      grund_status: TEXT*1;
      loesch_status: TEXT*1;
      quota_s: TEXT*300;
    UNIQUE rht_id;
    END recht;

  END eigentumsauskunft;

END ch_bl_zrv_eigentumsauskunft_v1_0.

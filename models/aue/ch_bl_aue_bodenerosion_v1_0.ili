INTERLIS 2.3;
!!========================================================================================
!! Copyright (c) 2021, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 11.03.2024 | 1.0     | lwi, GeoWerkstatt   | Initiale Erstellung gem. Modell WMS/GDWH
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_aue_bodenerosion_v1_0 (de) AT "https://models.geo.bl.ch/AUE/int" VERSION "2024-03-11" =

  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS bl_basis_kgdm_lv95_v2_0;

  TOPIC ch_bl_aue_bodenerosion =

    CLASS gefaehrdungsstufe =
      nutzung : TEXT*80;
      grad_gefaehrdung : TEXT*80;
      wahrscheinlichste_gefaehrdung : TEXT*100;
      kommentar : TEXT*200;
      aussagesicherheit : TEXT*50;
      hauptgrund_hohe_gefaehrdung : TEXT*100;
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
      gridcode : 0 .. 99999;
    END gefaehrdungsstufe;

    CLASS gewaesseranschluss =
      art : TEXT*50;
      ziel_massnahmen : TEXT*60;
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
    END gewaesseranschluss;
    
    CLASS sonstige_landnutzung =
      art : TEXT*50;
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
    END sonstige_landnutzung;

    CLASS details_erosion =
      bodenabtrag_1_wahrscheinlichk : TEXT*50;
      bodenabtrag_2_wahrscheinlichk : TEXT*50;
      kommentar : TEXT*200;
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
      ausssagesicherheit : TEXT*50;
    END details_erosion;

    CLASS details_gewaesseranschluss =
      art_gewaesseranschluss : TEXT*50;
      variante_anschluss : TEXT*80;
      besonderheit : TEXT*80;
      art_variante : TEXT*152;
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
      grund : TEXT*50;
      art : TEXT*70;
    END details_gewaesseranschluss;

    CLASS strasse_mit_einlaufschaechten = 
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLLinie2D;
    END strasse_mit_einlaufschaechten;

  END ch_bl_aue_bodenerosion;

END ch_bl_aue_bodenerosion_v1_0.

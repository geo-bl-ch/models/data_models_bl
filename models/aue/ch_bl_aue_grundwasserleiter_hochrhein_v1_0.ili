INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2023, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 11.09.2024 | 1.0     | Erich Brumann       | Ersterstellung
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_aue_grundwasserleiter_hochrhein_v1_0 (de) AT "http://models.geo.bl.ch/AUE/" VERSION "2024-09-11" =

  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0;

  TOPIC grundwasserleiter_hochrhein =
  	
    CLASS aquiferbasis =
      geometrie:        MANDATORY BLFlaeche2D;
	    hoehe:            BLHoehe;
    END aquiferbasis;

    CLASS aquiferlinie =
      geometrie:        MANDATORY BLLinie2D;
	    hoehe:            BLHoehe;
    END aquiferlinie;

    CLASS flurabstand =
      geometrie:        MANDATORY BLFlaeche2D;
	    abstand:          0 .. 2147483647;
    END flurabstand;

    CLASS grundwasser_machtigkeit =
      geometrie:                    MANDATORY BLFlaeche2D;
	    maechtigkeit:                 0 .. 2147483647;
    END grundwasser_machtigkeit;

    CLASS perimeter =
      geometrie:      MANDATORY BLFlaeche2D;
    END perimeter;

    CLASS karst_gw =
      geometrie:     MANDATORY BLFlaeche2D;
    END karst_gw;

    CLASS gw_gleichen =
      geometrie:       MANDATORY BLFlaeche2D;
	    hoehe:           BLHoehe;
    END gw_gleichen;

    CLASS gw_gleichenlin =
      geometrie:          MANDATORY BLLinie2D;
	    hoehe:              BLHoehe;
    END gw_gleichenlin;

    CLASS tektonik =
      geometrie:    MANDATORY BLLinie2D;
	    art:          TEXT*50;
    END tektonik;

    CLASS terrasse =
      geometrie:     MANDATORY BLLinie2D;
    END terrasse;

!! CLASS geoleinheit wird nicht ins Modell uebernommen

  END grundwasserleiter_hochrhein;

END ch_bl_aue_grundwasserleiter_hochrhein_v1_0.
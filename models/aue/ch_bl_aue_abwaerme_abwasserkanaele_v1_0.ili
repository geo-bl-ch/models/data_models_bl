INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2024, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 06.09.2024 | 1.0     | ogr, GeoWerkstatt   | Initiale Erstellung gem. Modell WMS/GDWH
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_aue_abwaerme_abwasserkanaele_katalog_v1_0 (de) AT "http://models.geo.bl.ch/AUE/" VERSION "2024-09-31" =

  IMPORTS CatalogueObjects_V1;

  TOPIC codelisten =

    CLASS nutzungsart
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*20;
      Text:  MANDATORY TEXT*40;
    END nutzungsart;

    STRUCTURE nutzungsart_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) nutzungsart;
    END nutzungsart_ref;

    CLASS status
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*20;
      Text:  MANDATORY TEXT*40;
    END status;

    STRUCTURE status_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) status;
    END status_ref;

    CLASS profil_typ
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*20;
      Text:  MANDATORY TEXT*40;
    END profil_typ;

    STRUCTURE profil_typ_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) profil_typ;
    END profil_typ_ref;

    CLASS hoehenbestimmung
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Code : MANDATORY TEXT*20;
      Text:  MANDATORY TEXT*40;
    END hoehenbestimmung;

    STRUCTURE hoehenbestimmung_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) hoehenbestimmung;
    END hoehenbestimmung_ref;

  END codelisten;

END ch_bl_aue_abwaerme_abwasserkanaele_katalog_v1_0.

MODEL ch_bl_aue_abwaerme_abwasserkanaele_v1_0 (de) AT "https://models.geo.bl.ch/AUE/" VERSION "2024-09-31" =
  
  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS bl_basis_kgdm_lv95_v2_0;
  IMPORTS ch_bl_aue_abwaerme_abwasserkanaele_katalog_v1_0;

  TOPIC ch_bl_aue_abwaerme_abwasserkanaele =
    DEPENDS ON ch_bl_aue_abwaerme_abwasserkanaele_katalog_v1_0.codelisten;

    CLASS abwaerme_abwasserkanaele =
      id : MANDATORY 1 .. 999999;
      bezeichnung : TEXT*200;
      nutzungsart : MANDATORY ch_bl_aue_abwaerme_abwasserkanaele_katalog_v1_0.codelisten.nutzungsart_ref;
      lagebestimmung : MANDATORY (genau,ungenau);
      status : MANDATORY ch_bl_aue_abwaerme_abwasserkanaele_katalog_v1_0.codelisten.status_ref;
      profil_typ : ch_bl_aue_abwaerme_abwasserkanaele_katalog_v1_0.codelisten.profil_typ_ref;
      profil_hoehe : 0 .. 10000;
      profil_breite : 0 .. 100000;
      anfangs_hoehe : bl_basis_kgdm_lv95_v2_0.BLHoehe;
      end_hoehe : bl_basis_kgdm_lv95_v2_0.BLHoehe;
      hoehenbestimmung : ch_bl_aue_abwaerme_abwasserkanaele_katalog_v1_0.codelisten.hoehenbestimmung_ref;
      quelle :(AIB_Haltungen,LK_Haltungen);
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLFlaeche2D;
    UNIQUE id;
    END abwaerme_abwasserkanaele;

  END ch_bl_aue_abwaerme_abwasserkanaele;

END ch_bl_aue_abwaerme_abwasserkanaele_v1_0.

INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2021, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in            | Aenderung
!!----------------------------------------------------------------------------------------
!! 18.09.2023 | 1.0     | ogr, GeoWerkstatt   | Initiale Erstellung gem. Modell WMS/GDWH
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_aue_gewaesserzustand_makrofauna_v1_0 (de) AT "https://models.geo.bl.ch/AUE" VERSION "2023-09-18" =
  
  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS bl_basis_kgdm_lv95_v2_0;

  TOPIC ch_bl_aue_gewaesserzustand_makrofauna =

    CLASS gesamtbeurteilung_messwerte_fruehling =
        probeentnahmestelle : TEXT*60;
        id : 0 .. 2147483647;
        datum : bl_basis_kgdm_lv95_v2_0.BLDatum;
        anzahl_taxa : 0 .. 2147483647;
        besiedlungsdichte : TEXT*60;
        biomasse : TEXT*60;
        makroindex : TEXT*60;
        ibgn : TEXT*60;
        saprobitaet : TEXT*60;
        geometrie : bl_basis_kgdm_lv95_v2_0.BLKoord2D;
        mzb_beurt_roh_id : 0 .. 2147483647;
        pnst_id : 0 .. 2147483647;
        anzahl_taxa_txt : TEXT*60;
        pop_dichte : 0 .. 2147483647;
        biomasse_ : 0.00 .. 2147483647.00;
        makroindex_note : 0 .. 2147483647;
        ibgn_note : 0 .. 2147483647;
        saprobitaet_note : 0 .. 2147483647;
        arten_txt : TEXT*6000;
    END gesamtbeurteilung_messwerte_fruehling;

    CLASS gesamtbeurteilung_messwerte_sommer =
        probeentnahmestelle : TEXT*60;
        id : 0 .. 2147483647;
        datum : bl_basis_kgdm_lv95_v2_0.BLDatum;
        anzahl_taxa : 0 .. 2147483647;
        besiedlungsdichte : TEXT*60;
        biomasse : TEXT*60;
        makroindex : TEXT*60;
        ibgn : TEXT*60;
        saprobitaet : TEXT*60;
        geometrie : bl_basis_kgdm_lv95_v2_0.BLKoord2D;
        mzb_beurt_roh_id : 0 .. 2147483647;
        pnst_id : 0 .. 2147483647;
        anzahl_taxa_txt : TEXT*60;
        pop_dichte : 0 .. 2147483647;
        biomasse_ : 0 .. 2147483647;
        makroindex_note : 0 .. 2147483647;
        ibgn_note : 0 .. 2147483647;
        saprobitaet_note : 0 .. 2147483647;
        arten_txt : TEXT*6000;
    END gesamtbeurteilung_messwerte_sommer;

  END ch_bl_aue_gewaesserzustand_makrofauna;

END ch_bl_aue_gewaesserzustand_makrofauna_v1_0.

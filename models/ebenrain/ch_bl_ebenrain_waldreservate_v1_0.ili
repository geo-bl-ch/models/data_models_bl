INTERLIS 2.3;

!!========================================================================================
!! Copyright (c) 2022, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in                     | Aenderung
!!----------------------------------------------------------------------------------------
!! 16.08.2022 | 1.0     | Philipp Franke               | Ersterstellung
!! 27.10.2022 | 1.0     | K. Deininger                 | Verwende bl_basis_kgdm_lv95_v2_0
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"
!!========================================================================================

MODEL ch_bl_ebenrain_waldreservate_v1_0 (de) AT "https://models.geo.bl.ch/EBENRAIN/" VERSION "2022-10-27" =

  IMPORTS UNQUALIFIED Units, CoordSys, INTERLIS;
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0;
  IMPORTS Waldreservate_Codelisten_V1_1;

  DOMAIN
    /** Werteliste zur Unterscheidung ob eine Eigentumsbeschränkung in Kraft ist oder nicht
        Kopiert aus OeREBKRM_V2_0.ili*/
    RechtsStatus = (inKraft);
    /** Werteliste für die Art der Unterschutzstellung, 'RRB'= Regierungsratsbeschluss oder 'Vertrag'= Dienstbarkeitsvertrag */
    EntscheidTyp = (
        RRB,
        Vertrag
    );

  TOPIC Waldreservate =
    DEPENDS ON Waldreservate_Codelisten_V1_1.Codelisten;

    CLASS Waldreservate =
      objekt_nr:              MANDATORY 1 .. 214783647;
      objekt_name:            MANDATORY TEXT*50;
      gesflaeche:             MANDATORY 0.00 .. 9999.99 [Units.ha];
      obj_gisflaeche:         0.00 .. 9999.99 [Units.ha];
      entscheid_datum:        BLDatum;
      entscheid_typ:          EntscheidTyp;
      lexlink:                MANDATORY 1 .. 214783674;
      rechtsstatus:           RechtsStatus;
    END Waldreservate;

    CLASS Waldreservate_Teilgebiete =
      id:                     MANDATORY 0 .. 214783647;
      teilflaeche_nr:         MANDATORY 1 .. 214783647;
      mcpfe_code:             MANDATORY Waldreservate_Codelisten_V1_1.Codelisten.MCPFE_Class_CatRef;
      teilobj_gisflaeche:     MANDATORY 0.00 .. 9999.99 [Units.ha];
      geometrie:              MANDATORY BLFlaeche2D;
      UNIQUE id;
    END Waldreservate_Teilgebiete;

    ASSOCIATION Waldreservat_Teilobjekt =
      wr_teilobj -- {1..*} Waldreservate_Teilgebiete;
      wr -<#> {1} Waldreservate;
    END Waldreservat_Teilobjekt;

  END Waldreservate;

END ch_bl_ebenrain_waldreservate_v1_0.

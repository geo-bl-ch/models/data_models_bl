TRANSFER INTERLIS1;
!!=========================================================================================================================================
!! Kanton Basel-Landschaft
!! Bau- und Umweltschutzdirektion
!! Amt f�r Raumplanung
!! Rauminformation
!! Rheinstrasse 29
!! 4410 Liestal
!! 
!! www.arp.bl.ch
!! 
!! Kantonales Geodatenmodell Baulinienpl�ne kommunal gem�ss Geobasisdatensatz Nr. 25-BL (inkl. Waldabstandslinien ID 159)
!!=========================================================================================================================================
!! Revision History
!! 
!! 2016.09.29 / ruck erstellt 
!! 2016.10.28 / ruck angepasst aufgrund R�ckmeldungen TBA + GIS-Fachstelle 
!! 2016.12.12 / ruck Anpassung Datentyp LexLink, Finale Version V2
!! 2019.06.01 / ruck Anpassung Einzelflaeche WITHOUT OVERLAPS > 0.050
!!=========================================================================================================================================
!!* @origin="Texteditor"
!!* @IDGeoIV=25-BL,159
!!* @author="Michael Ruckstuhl"
!!* @Modelltyp=Produktionsmodell
!!* @�REB-Rahmenmodell=Schnittstellenmodell
!!* @Issuer http://models.geo.bl.ch/ARP/
!!* @Version 2019-06-01
!!=========================================================================================================================================

!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="http://www.arp.bl.ch"
MODEL Baulinien_kommunal_BL_V2_LV95

  DOMAIN

    LKoord = COORD2 2480000.000  1070000.000
                    2850000.000  1310000.000;

	Orientierung = DEGREES 0.0 359.9; !! geografische Notation (90� = Horizontal West nach Ost)

    Schriftgroesse = (
      klein,                                                              !! 0
      mittel,                                                             !! 1
      gross);                                                             !! 2

      Linie = POLYLINE WITH (ARCS, STRAIGHTS) VERTEX LKoord;
      Einzelflaeche = SURFACE WITH (ARCS, STRAIGHTS) VERTEX LKoord WITHOUT OVERLAPS > 0.050;
      INTEGER10   = [1..2147483647];

      Entscheid = (
        ausstehend,
        bewilligt,
        nicht_bewilligt);

      PlanInstrument = (      !! Hauptinstrument der Planung, welche die Baulinien beschlossen hat
        unbekannt,
        BSP_kommunal,         !! kommmunaler Bau- und Strassenlinienplan,       
                              !! beinhaltet auch Waldbaulinien (Gemeinde)       
        QP,                   !! Im Quartierplan erlassene Baulinien (Gemeinde) 
        TZP,                  !! Im Teilzonenplan erlassene Baulinien (Gemeinde)
        GU,                   !! In Gesamtueberbauungen inkl Ueberbauungs-      
                              !! ordnungen erlassene Baulinien (Gemeinde)       
        ZPS,                  !! Im Zonenplan Siedlung erlassene                
                              !! Baulinien (Gemeinde)                           
        ZPL);                 !! Im Zonenplan Landschaft erlassene              
                              !! Baulinien (Gemeinde)                           

      TypMass = (
        Distanz,
        Winkel,
        Radius,
        Hilfslinie);

      BaulinieTyp = (
        Strassenbaulinie,
        Waldbaulinie,
        Gewaesserbaulinie,
        Schienenwegbaulinie,
        Gestaltungsbaulinie,
        Leitungsbaulinie,
        Schutzzonenbaulinie,
        Friedhofbaulinie,
        Laermschutzbaulinie);

      BaulinieGeltungsbereich = (
        Allgemein,                          !! kein spezieller Geltungsbereich
        U,                                  !! unterirdische_Baulinie
        S,                                  !! Stockwerkbaulinie
        A,                                  !! Arkadenbaulinie
        B,                                  !! Balkonbaulinie
        W);                                 !! Weitere

      VerkehrsflaecheTyp = (
        Strasse_Weg,
        Platz,
        Parkierungsflaeche,
        Bahntrassee,
        Gewaesser);

      VerkehrsflaecheGliederung = (
        Fahrbahn,
        Bankett,
        Radstreifen,
        Bushaltestelle,
        Parkplatz,
        Gruenstreifen,
        Gehweg,
        Radweg,
        Geh_und_Radweg,
        Trottoir,
        Perron,
        weitere);

      VerkehrsachseTyp = (
        Strassenachse,
        Schienenachse,
        Gewaesserachse);

!!-------------------------------------------------------------------------------


  TOPIC Baulinien =

    TABLE Beschluss =
      Plan_Nr:           OPTIONAL TEXT*40;  !! Inventarnummer
      Plan_Name:         OPTIONAL TEXT*100; !! Planname
      Instrument:        PlanInstrument;    !! 
      Gemeinde_ID_BFS:   OPTIONAL [2500..2900]; !! BFS Nummer der Gemeinde
      Beschluss_Nr:      OPTIONAL TEXT*40;  !! Nummer des Gemeindebeschlusses 
      Beschluss_Datum:   OPTIONAL DATE;     !! Datum des Gemeindebeschlusses [JJJJMMDD]
      Genehmigung_Nr:    OPTIONAL TEXT*40;  !! RRB Nummer
      Genehmigung_Datum: OPTIONAL DATE;     !! Datum des RRB [JJJJMMDD]
      Erfassung_durch:   TEXT*50;           !! Digitale Erfassung durch
                                            !! [Firma/Kuerzel]
      Erfassung_Datum:   DATE;              !! Datum der Digitalisation
      LexLink:        OPTIONAL INTEGER10;   !! Entscheid-ID gem�ss �REBlex
	  Bemerkungen:       OPTIONAL TEXT*254; !! Bemerkungen
    NO IDENT
    END Beschluss;

    TABLE GeometrieZuBeschluss =
      Beschl_Entscheid: Entscheid;      !! Gemeinde-Beschlussentscheid
      Genehm_Entscheid: Entscheid;      !! Genehmigungsentscheid (Regierungsrat)
      Erwaegungen:      (Nein, siehe_Erwaegungen_RRB); !! 0: Nein,
                                        !! 1: siehe Erwaegungen RRB
      Beschluss:        -> Beschluss;   !! Verknuepfung ueber die Interlis-OID 
                                        !! der Tabelle Beschluss
      Bemerkungen:      OPTIONAL TEXT*254;   !! Bemerkungen
    NO IDENT
    END GeometrieZuBeschluss;

    TABLE BeschlussPerimeter =
      Beschluss:        -> Beschluss;       !! Perimeter gehoert zu Beschluss 
                                            !! (Interlis-OID)
      Bemerkungen:      OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie:        Einzelflaeche;      !! Geometrie, Typ Polygon
    NO IDENT
    END BeschlussPerimeter;

    TABLE ErwaegungPos =
      Objekt:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die Interlis-OID 
                                            !! der Tabelle GeometrieZuBeschluss
      HAli:        HALIGNMENT;     !! horizontale Ausrichtung
      VAli:        VALIGNMENT;     !! vertikale Ausrichtung
      Ori:         Orientierung;       !! Orientierung
      Groesse:     Schriftgroesse; !! 0:klein, 1:mittel, 2:gross
      Geometrie:   LKoord;
    NO IDENT
    END ErwaegungPos;

    TABLE Baulinie =
      BL_OID:          OPTIONAL INTEGER10; !! Kantonsinterner Identifikator
      BL_OP_ID:        INTEGER10;          !! Gemeindeinterner Identifikator
                                           !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Baulinie wurde 
                                                         !! aufgehoben mit 
                                                         !! GeometrieZuBeschluss
      Provisorisch:    (nein, ja);          !! wenn provisorische Baulinie: ja
      Typ:             BaulinieTyp;        !! Aufzaehlung
      Geltungsbereich: BaulinieGeltungsbereich; !! Aufzaehlung
      GeltungsbereichBez: TEXT*254;        !! Geltungsbereichs-Bezeichnung
      Bemerkungen:     OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie :      Linie;              !! Die Baulinie ist eine gerichtete 
                                           !! Linie. Rechts der Baulinie darf 
                                           !! gebaut werden.
    NO IDENT
    END Baulinie;

    TABLE BemassungBaulinie =
      Baulinie:    -> Baulinie;    !! Verknuepfung ueber die Interlis-OID 
                                   !! der Tabelle Baulinie
      Mass:        TEXT*20;        !! Mass: 2.50 m
      Typ:         TypMass;        !! Aufzaehlung
      Bemerkungen: OPTIONAL TEXT*254; !! Bemerkungen
      Geometrie:   Linie;
    NO IDENT
    END BemassungBaulinie;

    TABLE BemassungBLPos =
      BemassungBaulinie: -> BemassungBaulinie;  !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! BemassungBaulinie
      HAli:        HALIGNMENT;     !! horizontale Ausrichtung
      VAli:        VALIGNMENT;     !! vertikale Ausrichtung
      Ori:         Orientierung;       !! Orientierung
      Groesse:     Schriftgroesse; !! 0:klein, 1:mittel, 2:gross
      Geometrie:   LKoord;
    NO IDENT
    END BemassungBLPos;

    TABLE Baufeld =
      BF_OID:          OPTIONAL INTEGER10;  !! Kantonsinterner Identifikator 
      BF_OP_ID:        INTEGER10;           !! Gemeindeinterner Identifikator
                                            !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Baufeld wurde 
                                                         !! aufgehoben mit 
                                                         !! GeometrieZuBeschluss
      Bemerkungen:     OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie :      Einzelflaeche;
    NO IDENT
    END Baufeld;

    TABLE BemassungBaufeld =
      Baufeld:          -> Baufeld;!! Verknuepfung ueber die Interlis-OID 
                                   !! der Tabelle Baufeld
      Mass:             TEXT*20;   !! Mass: 2.5 m
      Typ:              TypMass;   !! Aufzaehlung
      Bemerkungen:      OPTIONAL TEXT*254; !! Bemerkungen
      Geometrie:        Linie;
    NO IDENT
    END BemassungBaufeld;

    TABLE BemassungBFPos =
      BemassungBaufeld:  -> BemassungBaufeld;  !! Position gehoert zu Bemassung
      HAli:        HALIGNMENT;     !! horizontale Ausrichtung
      VAli:        VALIGNMENT;     !! vertikale Ausrichtung
      Ori:         Orientierung;       !! Orientierung
      Groesse:     Schriftgroesse; !! 0:klein, 1:mittel, 2:gross
      Geometrie:   LKoord;
    NO IDENT
    END BemassungBFPos;

    TABLE Strassenlinie =
      SL_OID:          OPTIONAL INTEGER10; !! Kantonsinterner Identifikator
      SL_OP_ID:        INTEGER10;          !! Gemeindeinterner Identifikator
                                           !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Strassenlinie wurde
                                                         !! aufgehoben mit 
                                                         !! GeometrieZuBeschluss
      Bemerkungen:     OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie :      Linie;              !! Die Strassenlinie ist eine 
                                           !! gerichtete Linie. Links der 
                                           !! Strassenlinie ist Verkehrs-
                                           !! fl�che.
    NO IDENT
    END Strassenlinie;

    TABLE BemassungStrassenlinie =
      Strassenlinie:   -> Strassenlinie;  !! Verknuepfung ueber die Interlis-OID 
                                          !! der Tabelle Strassenlinie
      Mass:            TEXT*20;           !! Mass: 2.50 m
      Typ:             TypMass;           !! Aufzaehlung
      Bemerkungen:     OPTIONAL TEXT*254; !! Bemerkungen
      Geometrie:       Linie;
    NO IDENT
    END BemassungStrassenlinie;

    TABLE BemassungSLPos =
      BemassungStrassenlinie: -> BemassungStrassenlinie; !! Verknuepfung ueber die
                                      !! Interlis-OID der Tabelle 
                                      !! BemassungStrassenlinie
      HAli:            HALIGNMENT;    !! horizontale Ausrichtung
      VAli:            VALIGNMENT;    !! vertikale Ausrichtung
      Ori:             Orientierung;      !! Orientierung
      Groesse:         Schriftgroesse;   !! 0:klein, 1:mittel, 2:gross
      Geometrie:       LKoord;
    NO IDENT
    END BemassungSLPos;

    OPTIONAL TABLE Verkehrsachse =
      VA_OID:          OPTIONAL INTEGER10;  !! Kantonsinterner Identifikator
      VA_OP_ID:        INTEGER10;           !! Gemeindeinterner Identifikator
                                            !! Bleibt stabil
      Typ:             VerkehrsachseTyp;    !! Aufzaehlung
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Strassenachse wurde
                                                         !! aufgehoben mit 
                                                         !! GeometrieZuBeschluss
      Bemerkungen:     OPTIONAL TEXT*254;   !! Bemerkungen
      Geometrie:       Linie;
    NO IDENT
    END Verkehrsachse;

    TABLE Verkehrsflaeche =
      VF_OID:          OPTIONAL INTEGER10;  !! Kantonsinterner Identifikator
      VF_OP_ID:        INTEGER10;           !! Gemeindeinterner Identifikator
                                            !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Verkehrsflaeche 
                                                         !! wurde aufgehoben mit
                                                         !! GeometrieZuBeschluss
      Typ:             VerkehrsflaecheTyp;        !! Aufzaehlung
      Gliederung:      VerkehrsflaecheGliederung; !! Aufzaehlung
      Bemerkungen:     OPTIONAL TEXT*254;   !! Bemerkungen
      Geometrie :      Einzelflaeche;
    NO IDENT
    END Verkehrsflaeche;

  END Baulinien.


!!-------------------------------------------------------------------------------


END Baulinien_kommunal_BL_V2_LV95.

FORMAT FREE;


CODE
  BLANK = DEFAULT, UNDEFINED = DEFAULT, CONTINUE = DEFAULT;
  TID = ANY;
END.

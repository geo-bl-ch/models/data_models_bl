TRANSFER INTERLIS1;
!!=========================================================================================================================================
!! Kanton Basel-Landschaft
!! Bau- und Umweltschutzdirektion
!! Tiefbauamt
!! Rheinstrasse 29
!! 4410 Liestal
!! 
!! 
!! Kantonales Geodatenmodell Baulinienpl�ne kantonal gem�ss Geobasisdatensatz Nr. 22-BL
!!=========================================================================================================================================
!! Revision History
!! 
!! 2016.09.29 / ruck erstellt 
!! 2016.10.28 / ruck angepasst aufgrund R�ckmeldungen TBA + GIS-Fachstelle 
!! 2016.12.09 / ruck Anpassung Datentyp LexLink, Finale Version V2
!!=========================================================================================================================================
!!* @origin="Texteditor"
!!* @IDGeoIV=22-BL
!!* @author="Michael Ruckstuhl"
!!* @Modelltyp=Produktionsmodell
!!* @�REB-Rahmenmodell=Schnittstellenmodell
!!* @Issuer http://models.geo.bl.ch/TBA/
!!* @Version 2017-01-01
!!=========================================================================================================================================

!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://tba.bl.ch/"
MODEL Baulinien_kantonal_BL_V2_LV95

  DOMAIN

    LKoord = COORD2 2480000.000  1070000.000
                    2850000.000  1310000.000;

	Orientierung = DEGREES 0.0 359.9; !! geografische Notation (90� = Horizontal West nach Ost)

    Schriftgroesse = (
      klein,                                                              !! 0
      mittel,                                                             !! 1
      gross);                                                             !! 2

      Linie = POLYLINE WITH (ARCS, STRAIGHTS) VERTEX LKoord;
      Einzelflaeche = SURFACE WITH (ARCS, STRAIGHTS) VERTEX LKoord;
      INTEGER10   = [1..2147483647];

      Entscheid = (
        ausstehend,
        bewilligt,
        nicht_bewilligt);

      PlanInstrument = (      !! Hauptinstrument der Planung, welche die Baulinien beschlossen hat
        unbekannt,
        BSP_kantonal,         !! Bau- und Straseenlinien an Kantonsstrassen und Gewaessern (Tiefbauamt)
        KNP);                 !! Mit kantonalem Nutzungsplan erlassene Baulinien (Kanton)

      TypMass = (
        Distanz,
        Winkel,
        Radius,
        Hilfslinie);

      BaulinieTyp = (
        Strassenbaulinie,
        Waldbaulinie,
        Gewaesserbaulinie,
        Schienenwegbaulinie,
        Gestaltungsbaulinie,
        Leitungsbaulinie,
        Schutzzonenbaulinie,
        Friedhofbaulinie,
        Laermschutzbaulinie);

      BaulinieGeltungsbereich = (
        Allgemein,                          !! kein spezieller Geltungsbereich
        U,                                  !! unterirdische_Baulinie
        S,                                  !! Stockwerkbaulinie
        A,                                  !! Arkadenbaulinie
        B,                                  !! Balkonbaulinie
        W);                                 !! Weitere

      VerkehrsflaecheTyp = (
        Strasse_Weg,
        Platz,
        Parkierungsflaeche,
        Bahntrassee,
        Gewaesser);

      VerkehrsflaecheGliederung = (
        Fahrbahn,
        Bankett,
        Radstreifen,
        Bushaltestelle,
        Parkplatz,
        Gruenstreifen,
        Gehweg,
        Radweg,
        Geh_und_Radweg,
        Trottoir,
        Perron,
        weitere);

      VerkehrsachseTyp = (
        Strassenachse,
        Schienenachse,
        Gewaesserachse);

!!-------------------------------------------------------------------------------


  TOPIC Baulinien =

    TABLE Beschluss =
      Plan_Nr:           OPTIONAL TEXT*40;  !! Plannummer zur Identifikation
      Plan_Name:         OPTIONAL TEXT*100; !! Planname
      Instrument:        PlanInstrument;    !! 
      Gemeinde_ID_BFS:   OPTIONAL [2500..2900]; !! BFS Nummer der Gemeinde, falls eine einzelne Gemeinde betroffen, ansonsten leer
      Beschluss_Nr:      OPTIONAL TEXT*40;  !! Nummer des BUDE des Beschlusses 
      Beschluss_Datum:   OPTIONAL DATE;     !! Datum des BUDE des Beschlusses
      Genehmigung_Nr:    OPTIONAL TEXT*40;  !! Nummer des BUDE der Rechtskraftbescheinigung
      Genehmigung_Datum: OPTIONAL DATE;     !! Datum des BUDE der Rechtskraftbescheinigung [JJJJMMDD]
      Erfassung_durch:   TEXT*50;           !! Digitale Erfassung durch
                                            !! [Firma/Kuerzel]
      Erfassung_Datum:   DATE;              !! Datum der Digitalisation
      LexLink:        OPTIONAL INTEGER10;   !! Link in den �REB-Kataster
	  Bemerkungen:       OPTIONAL TEXT*254; !! Bemerkungen
    NO IDENT
    END Beschluss;

    TABLE GeometrieZuBeschluss =
      Beschl_Entscheid: Entscheid;      !! Beschlussentscheid 
      Genehm_Entscheid: Entscheid;      !! Rechtskraftbescheinigung
      Erwaegungen:      (Nein, siehe_Erwaegungen_RRB); !! 0: Nein,
                                        !! 1: siehe Erwaegungen RRB
      Beschluss:        -> Beschluss;   !! Verknuepfung ueber die Interlis-OID 
                                        !! der Tabelle Beschluss
      Bemerkungen:      OPTIONAL TEXT*254;   !! Bemerkungen
    NO IDENT
    END GeometrieZuBeschluss;

    TABLE BeschlussPerimeter =
      Beschluss:        -> Beschluss;       !! Perimeter gehoert zu Beschluss 
                                            !! (Interlis-OID)
      Bemerkungen:      OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie:        Einzelflaeche;      !! Geometrie, Typ Polygon
    NO IDENT
    END BeschlussPerimeter;

    OPTIONAL TABLE ErwaegungPos =
      Objekt:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die Interlis-OID 
                                            !! der Tabelle GeometrieZuBeschluss
      HAli:        HALIGNMENT;     !! horizontale Ausrichtung
      VAli:        VALIGNMENT;     !! vertikale Ausrichtung
      Ori:         Orientierung;       !! Orientierung
      Groesse:     Schriftgroesse; !! 0:klein, 1:mittel, 2:gross
      Geometrie:   LKoord;
    NO IDENT
    END ErwaegungPos;

    TABLE Baulinie =
      BL_OID:          OPTIONAL INTEGER10; !! Kantonsinterner Identifikator
      BL_OP_ID:        INTEGER10;          !! Gemeindeinterner Identifikator
                                           !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Baulinie wurde 
                                                         !! aufgehoben mit 
                                                         !! GeometrieZuBeschluss
      Provisorisch:    (nein, ja);          !! wenn provisorische Baulinie: ja
      Typ:             BaulinieTyp;        !! Aufzaehlung
      Geltungsbereich: BaulinieGeltungsbereich; !! Aufzaehlung
      GeltungsbereichBez: TEXT*254;        !! Geltungsbereichs-Bezeichnung
      Bemerkungen:     OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie :      Linie;              !! Die Baulinie ist eine gerichtete 
                                           !! Linie. Rechts der Baulinie darf 
                                           !! gebaut werden.
    NO IDENT
    END Baulinie;

    TABLE BemassungBaulinie =
      Baulinie:    -> Baulinie;    !! Verknuepfung ueber die Interlis-OID 
                                   !! der Tabelle Baulinie
      Mass:        TEXT*20;        !! Mass: 2.50 m
      Typ:         TypMass;        !! Aufzaehlung
      Bemerkungen: OPTIONAL TEXT*254; !! Bemerkungen
      Geometrie:   Linie;
    NO IDENT
    END BemassungBaulinie;

    TABLE BemassungBLPos =
      BemassungBaulinie: -> BemassungBaulinie;  !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! BemassungBaulinie
      HAli:        HALIGNMENT;     !! horizontale Ausrichtung
      VAli:        VALIGNMENT;     !! vertikale Ausrichtung
      Ori:         Orientierung;       !! Orientierung
      Groesse:     Schriftgroesse; !! 0:klein, 1:mittel, 2:gross
      Geometrie:   LKoord;
    NO IDENT
    END BemassungBLPos;

    TABLE Baufeld =
      BF_OID:          OPTIONAL INTEGER10;  !! Kantonsinterner Identifikator 
      BF_OP_ID:        INTEGER10;           !! Gemeindeinterner Identifikator
                                            !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Baufeld wurde 
                                                         !! aufgehoben mit 
                                                         !! GeometrieZuBeschluss
      Bemerkungen:     OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie :      Einzelflaeche;
    NO IDENT
    END Baufeld;

    TABLE BemassungBaufeld =
      Baufeld:          -> Baufeld;!! Verknuepfung ueber die Interlis-OID 
                                   !! der Tabelle Baufeld
      Mass:             TEXT*20;   !! Mass: 2.5 m
      Typ:              TypMass;   !! Aufzaehlung
      Bemerkungen:      OPTIONAL TEXT*254; !! Bemerkungen
      Geometrie:        Linie;
    NO IDENT
    END BemassungBaufeld;

    TABLE BemassungBFPos =
      BemassungBaufeld:  -> BemassungBaufeld;  !! Position gehoert zu Bemassung
      HAli:        HALIGNMENT;     !! horizontale Ausrichtung
      VAli:        VALIGNMENT;     !! vertikale Ausrichtung
      Ori:         Orientierung;       !! Orientierung
      Groesse:     Schriftgroesse; !! 0:klein, 1:mittel, 2:gross
      Geometrie:   LKoord;
    NO IDENT
    END BemassungBFPos;

    TABLE Strassenlinie =
      SL_OID:          OPTIONAL INTEGER10; !! Kantonsinterner Identifikator
      SL_OP_ID:        INTEGER10;          !! Gemeindeinterner Identifikator
                                           !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Strassenlinie wurde
                                                         !! aufgehoben mit 
                                                         !! GeometrieZuBeschluss
      Bemerkungen:     OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie :      Linie;              !! Die Strassenlinie ist eine 
                                           !! gerichtete Linie. Links der 
                                           !! Strassenlinie ist Verkehrs-
                                           !! fl�che.
    NO IDENT
    END Strassenlinie;

    TABLE BemassungStrassenlinie =
      Strassenlinie:   -> Strassenlinie;  !! Verknuepfung ueber die Interlis-OID 
                                          !! der Tabelle Strassenlinie
      Mass:            TEXT*20;           !! Mass: 2.50 m
      Typ:             TypMass;           !! Aufzaehlung
      Bemerkungen:     OPTIONAL TEXT*254; !! Bemerkungen
      Geometrie:       Linie;
    NO IDENT
    END BemassungStrassenlinie;

    TABLE BemassungSLPos =
      BemassungStrassenlinie: -> BemassungStrassenlinie; !! Verknuepfung ueber die
                                      !! Interlis-OID der Tabelle 
                                      !! BemassungStrassenlinie
      HAli:            HALIGNMENT;    !! horizontale Ausrichtung
      VAli:            VALIGNMENT;    !! vertikale Ausrichtung
      Ori:             Orientierung;      !! Orientierung
      Groesse:         Schriftgroesse;   !! 0:klein, 1:mittel, 2:gross
      Geometrie:       LKoord;
    NO IDENT
    END BemassungSLPos;

    TABLE Bahntrasseelinie =
      TL_OID:          OPTIONAL INTEGER10; !! Kantonsinterner Identifikator
      TL_OP_ID:        INTEGER10;          !! Gemeindeinterner Identifikator
                                           !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Bahntrasseelinie 
                                                         !! wurde aufgehoben mit
                                                         !! GeometrieZuBeschluss
      Bemerkungen:     OPTIONAL TEXT*254;  !! Bemerkungen
      Geometrie :      Linie;
    NO IDENT
    END Bahntrasseelinie;

    TABLE Verkehrsachse =
      VA_OID:          OPTIONAL INTEGER10;  !! Kantonsinterner Identifikator
      VA_OP_ID:        INTEGER10;           !! Gemeindeinterner Identifikator
                                            !! Bleibt stabil
      Typ:             VerkehrsachseTyp;    !! Aufzaehlung
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Strassenachse wurde
                                                         !! aufgehoben mit 
                                                         !! GeometrieZuBeschluss
      Bemerkungen:     OPTIONAL TEXT*254;   !! Bemerkungen
      Geometrie:       Linie;
    NO IDENT
    END Verkehrsachse;

    TABLE Verkehrsflaeche =
      VF_OID:          OPTIONAL INTEGER10;  !! Kantonsinterner Identifikator
      VF_OP_ID:        INTEGER10;           !! Gemeindeinterner Identifikator
                                            !! Bleibt stabil
      Entstehung:      -> GeometrieZuBeschluss; !! Verknuepfung ueber die 
                                                !! Interlis-OID der Tabelle 
                                                !! GeometrieZuBeschluss
      Aufhebung:       OPTIONAL -> GeometrieZuBeschluss; !! Verkehrsflaeche 
                                                         !! wurde aufgehoben mit
                                                         !! GeometrieZuBeschluss
      Typ:             VerkehrsflaecheTyp;        !! Aufzaehlung
      Gliederung:      VerkehrsflaecheGliederung; !! Aufzaehlung
      Bemerkungen:     OPTIONAL TEXT*254;   !! Bemerkungen
      Geometrie :      Einzelflaeche;
    NO IDENT
    END Verkehrsflaeche;

  END Baulinien.


!!-------------------------------------------------------------------------------


END Baulinien_kantonal_BL_V2_LV95.

FORMAT FREE;


CODE
  BLANK = DEFAULT, UNDEFINED = DEFAULT, CONTINUE = DEFAULT;
  TID = ANY;
END.

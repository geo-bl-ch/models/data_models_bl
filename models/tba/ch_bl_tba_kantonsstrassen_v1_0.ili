INTERLIS 2.3;
!!========================================================================================
!! Copyright (c) 2021, GIS-Fachstelle des Amtes für Geoinformation Kanton Basel-Landschaft
!! All rights reserved.
!!
!! Datum      | Version | Autor/in             | Aenderung
!!----------------------------------------------------------------------------------------
!! 21.07.2024 | 1.0     | RUDERT-GEOINFORMATIK | Ersterstellung
!! 19.08.2024 | 1.0     | Philipp Franke | Anpassung Attributnamen
!!----------------------------------------------------------------------------------------
!!@ technicalContact="mailto:support.gis@bl.ch"
!!@ furtherInformation="https://geo.bl.ch"

MODEL ch_bl_tba_kantonsstrassen_v1_0 (de) AT "http://models.geo.bl.ch/TBA/" VERSION "2024-07-21" =
  IMPORTS UNQUALIFIED bl_basis_kgdm_lv95_v2_0;
  IMPORTS CatalogueObjects_V1;

  TOPIC codelisten =
    CLASS klasse_item
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Text: MANDATORY TEXT*50;
    END klasse_item;

    STRUCTURE klasse_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) klasse_item;
    END klasse_ref;

    CLASS pos_code_item
    EXTENDS CatalogueObjects_V1.Catalogues.Item =
      Text: MANDATORY TEXT*20;
    END pos_code_item;

    STRUCTURE pos_code_ref
    EXTENDS CatalogueObjects_V1.Catalogues.CatalogueReference =
      Reference (EXTENDED) : REFERENCE TO (EXTERNAL) pos_code_item;
    END pos_code_ref;
  END codelisten;

  TOPIC ch_bl_tba_kantonsstrassen =
    DEPENDS ON ch_bl_tba_kantonsstrassen_v1_0.codelisten;

    CLASS achse =
      id : MANDATORY 1 .. 2147483647;
      axe_part_oid : 0 .. 2147483647;
      achsenname : TEXT*100;
      achsenbezeichnung : TEXT*200;
      strassenname : TEXT*200;
      klasse : ch_bl_tba_kantonsstrassen_v1_0.codelisten.klasse_ref;
      segment_laenge : 0.00 .. 9999.99;
      kalibrationsfaktor : 0.000000 .. 9.999999;
      bezugspunkt_start : 0 .. 2147483647;
      bezugspunkt_ende : 0 .. 2147483647;
      positionscode_achse : ch_bl_tba_kantonsstrassen_v1_0.codelisten.pos_code_ref;
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLLinie2D;
    END achse;

    CLASS basisbezugspunkt =
      id : MANDATORY 1 .. 2147483647;
      bezugspunkt_oid : 0 .. 2147483647;
      bezugspunktname : TEXT*50;
      positionscode_bezugspunkt : 0 .. 2147483647;
      bezugspunkt_ori : -360 .. 360;
      achsenname : TEXT*100;
      achsenbezeichnung : TEXT*200;
      geometrie : MANDATORY bl_basis_kgdm_lv95_v2_0.BLKoord2D;
    END basisbezugspunkt;

  END ch_bl_tba_kantonsstrassen;

END ch_bl_tba_kantonsstrassen_v1_0.

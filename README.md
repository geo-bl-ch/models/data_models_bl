Datenmodelle Basel-Landschaft
=============================

Dieses Repository enthält die Datenmodelle das Kantons Basel-Landschaft. Ziel ist es, hier
alle Modelle abzulegen und zu versionieren. Damit soll sicher gestellt werden, dass
Änderungen über einen langen Zeitraum nachvollziehbar bleiben.

Vorgaben zur Struktur
---------------------

Alle aktuell gültigen Datenmodelle werden im Verzeichnis *models* und im Unterverzeichnis
der betreffenden Amtsstelle (Ordnername = Abkürzung des Amts (Kleinschreibung)) abgelegt. Falls eine Amtsstelle noch nicht existiert, muss das Unterverzeichnis neu erstellt werden und die Übersetzung der Abkürzung zum vollständigen Namen in der Datei `index/index.xslt` eingetragen werden.

Alte Versionen von Datenmodellen werden im jeweiligen Amts-Unterverzeichnis in das Verzeichnis
*ersetzt* verschoben. So wird sichergestellt, dass alte Modellversionen für Endnutzer
verfügbar bleiben.

Vorgaben zur Benennung von Modellen
-----------------------------------

Bestehende Datenmodelle (23.09.2020) und ihre alten Versionen wurden vom Namen her
1:1 übernommen.

Neue Datenmodelle folgen dem Namensschema der Datenintegration. Da Modellnamen keine
`.` oder `-` enthalten dürfen, werden diese Zeichen ersetzt.

Beispiel:
_________

Aus:

`ch.bfs.volkszaehlung-gebaeudestatistik_gebaeude_v1_0`

Wird:

`ch_bfs_volkszaehlung_gebaeudestatistik_gebaeude_v1_0`

Es ist sicherzustellen, dass zu jedem Datenmodell im Verzeichnis *latest* immer nur
eine Version liegt.

Vorgaben zur Version
--------------------

Die Versionsbenennung erfolgt nach https://semver.org/ bildet aber nur die ersten
beiden Stellen *major* und *minor* ab. Stellt eine Modellanpassung einen
_Breaking Change_ dar ist die *major* Ziffer um 1 zu erhöhen. Bei kleineren
Änderungen wird die *minor* Ziffer um 1 erhöht.

Publikation auf models.geo.bl.ch
--------------------------------

Eine neue komplett integrierte und saubere Version der Modellablage wird daraufhin automatisch durch die GitLab Ci erzeugt wo sie von OpenShift (schaut alle 15 Minuten ob es Änderungen gab) abgeholt und deployt wird.



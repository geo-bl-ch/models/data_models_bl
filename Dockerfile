FROM node:18-alpine AS css

RUN mkdir /data && \
    cd /data && \
    npm install sass bootstrap@5.1.3 font-awesome@4.7.0

COPY scss /data

WORKDIR /data

RUN ./node_modules/.bin/sass index.scss index.css

FROM nginx:1.26-alpine-perl

USER 0

RUN apk update && apk upgrade

COPY nginx.conf /etc/nginx/nginx.conf
COPY models /data
COPY index /data/_index
COPY index/favicon.ico /data/favicon.ico
COPY --from=css /data/index.css /data/_index/css/index.css
COPY --from=css /data/index.css.map /data/_index/css/index.css.map
COPY --from=css /data/node_modules/font-awesome/fonts/* /data/_index/fonts/

EXPOSE 8080

CMD ["nginx", "-g", "daemon off; load_module /etc/nginx/modules/ngx_http_perl_module.so; load_module modules/ngx_http_xslt_filter_module.so;"]

<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" />

    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>

        <html>
            <head>
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta charset="UTF-8" />
                <meta name="viewport" content="initial-scale=1, shrink-to-fit=no, viewport-fit=cover, width=device-width, height=device-height" />
                <title>Datenmodelle, Kanton Basel-Landschaft</title>
            </head>
            <body>

                <div class="container shadow bg-white pt-3 pb-3">

                    <p class="float-end pb-3">
                        <a id="logo" href="https://www.baselland.ch/"></a>
                    </p>
                    <div class="clearfix"></div>
                    <h1>Modellablage Geobasisdaten</h1>
                    <hr class="border-danger" />

                    <p class="mt-3 mb-0">
                        Modellablage für Datenmodelle der Geobasisdaten nach Bundesrecht in Zuständigkeit des Kantons Basel-Landschaft und nach kantonalem Recht.
                    </p>

                    <div>
                        <div class="row pt-3 pb-1 mb-0">
                            <div class="col-md-5">Kontakt:&#160;&#160;<a href="mailto:support.gis@bl.ch" class="link-primary"><i class="fa fa-envelope-o" style="padding-right: 0.25em;"></i>Amt für Geoinformation BL</a></div>
                        </div>
                    </div>

                    <ol class="breadcrumb mt-3 bg-white font-weight-bolder"></ol>

                    <xsl:if test="count(list/*[not(. = '_index') and not(. = 'version.txt') and name(.) = 'directory']) > 0">
                        <div class="card bg-light mt-3">
                            <div class="card-body">
                                <ul class="list-unstyled mb-0" id="directory-list">
                                    <xsl:for-each select="list/*[not(. = '_index') and not(. = 'version.txt') and not(. = 'favicon.ico') and name(.) = 'directory']">
                                        <li>
                                            <xsl:attribute name="class">
                                                <xsl:value-of select="name(.)" />
                                            </xsl:attribute>

                                            <i class="fa fa-folder-o" aria-hidden="true"></i>&#160;
                                            <a>
                                                <xsl:attribute name="href">
                                                    <xsl:value-of select="." />
                                                    <xsl:text>/</xsl:text>
                                                </xsl:attribute>

                                                <xsl:text></xsl:text>
                                                <xsl:choose>
                                                <xsl:when test=". = 'afg'">
                                                    <xsl:value-of select="'Amt für Gesundheit (AfG)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'afk'">
                                                    <xsl:value-of select="'Amt für Kultur (AfK)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'afww'">
                                                    <xsl:value-of select="'Amt für Wald und Wild beider Basel (AfWW)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'agi'">
                                                    <xsl:value-of select="'Amt für Geoinformation (AGI)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'amb'">
                                                    <xsl:value-of select="'Amt für Militär und Bevölkerungsschutz (AMB)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'aue'">
                                                    <xsl:value-of select="'Amt für Umweltschutz und Energie (AUE)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'arp'">
                                                    <xsl:value-of select="'Amt für Raumplanung (ARP)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'bfs'">
                                                    <xsl:value-of select="'Bundesamt für Statistik (BfS)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'bl'">
                                                    <xsl:value-of select="'Kanton Basel-Landschaft (BL)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'ebenrain'">
                                                    <xsl:value-of select="'Ebenrain-Zentrum für Landwirtschaft, Natur und Ernährung (Ebenrain)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'lha'">
                                                    <xsl:value-of select="'Lufthygieneamt beider Basel (LHA)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'snzbb'">
                                                    <xsl:value-of select="'Sanitätsnotrufzentrale beider Basel (SNZBB)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'afds'">
                                                    <xsl:value-of select="'Amt für Daten und Statistik (AfDS)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'tba'">
                                                    <xsl:value-of select="'Tiefbauamt (TBA)'" />
                                                </xsl:when>
                                                <xsl:when test=". = 'zrv'">
                                                    <xsl:value-of select="'Zivilrechtsverwaltung (ZRV)'" />
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="." />
                                                </xsl:otherwise>
                                                </xsl:choose>
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </div>
                        </div>
                    </xsl:if>

                    <xsl:if test="count(list/*[not(. = '_index') and not(. = 'version.txt') and name(.) = 'file']) > 0">
                        <div class="card bg-light mt-3">
                            <div class="card-body">
                                <ul class="list-unstyled mb-0">
                                    <xsl:for-each select="list/*[not(. = '_index') and not(. = 'version.txt') and not(. = 'favicon.ico') and name(.) = 'file']">
                                        <li>
                                            <xsl:attribute name="class">
                                                <xsl:value-of select="name(.)" />
                                            </xsl:attribute>

                                            <i class="fa fa-file-o" aria-hidden="true"></i>&#160;
                                            <a>
                                                <xsl:attribute name="href">
                                                    <xsl:value-of select="." />
                                                </xsl:attribute>

                                                <xsl:text></xsl:text>
                                                <xsl:value-of select="." />
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </div>
                        </div>
                    </xsl:if>

                </div>

            </body>

            <footer class="text-center text-muted pt-3 pb-3">
                <small>Copyright 2020, Kanton Basel-Landschaft</small>
            </footer>

            <script>
                var urlParts = location.href.split("/", 3);
                if (urlParts.length > 3) urlParts.pop();
                var url = urlParts.join("/").toLowerCase();


                var css = document.createElement("link");
                css.href = url + "/_index/css/index.css";
                css.type = "text/css";
                css.rel = "stylesheet";

                var icon = document.createElement("link");
                icon.href = url + "/_index/favicon.ico";
                icon.type = "image/x-icon";
                icon.rel = "shortcut icon";

                var logo = document.createElement("img");
                logo.src = url + "/_index/logo.png";
                logo.height = 55;

                document.getElementsByTagName("head")[0].appendChild(css);
                document.getElementsByTagName("head")[0].appendChild(icon);
                document.getElementById("logo").appendChild(logo);

                var breadcrumbs = [
                    {
                        text: location.hostname.toLowerCase(),
                        url: url
                    }
                ];

                for (var part of location.pathname.substring(1).split("/")) {
                    if (part !== "") {
                        breadcrumbs.push({
                            text: part.toLowerCase(),
                            url: breadcrumbs[breadcrumbs.length-1]["url"] + "/" + part.toLowerCase() + "/"
                        });
                    }
                }

                var breadcrumb = document.getElementsByClassName("breadcrumb")[0];

                for (var part of breadcrumbs) {
                    var li = document.createElement("li");
                    li.classList.add("breadcrumb-item");
                    if (part === breadcrumbs[breadcrumbs.length-1]) {
                        li.classList.add("active");
                        li.textContent = part["text"].toLowerCase();
                    }
                    else {
                        var a = document.createElement("a");
                        a.href = part["url"];
                        a.textContent = part["text"];
                        li.appendChild(a);
                    }
                    breadcrumb.appendChild(li);
                }

                // Sort directory list: (1) Kanton BL (2)-(n) Ämter BL
                var ul = document.getElementById('directory-list');
                if (ul !== null){
                    Array.from(ul.getElementsByTagName("LI"))
                        .sort((a,b) => {
                            if (a.textContent.includes("Kanton Basel-Landschaft (BL)")){
                                return -1;
                            } else if (b.textContent.includes("Kanton Basel-Landschaft (BL)")){
                                return 1;
                            } else {
                                return a.textContent.localeCompare(b.textContent)
                            }
                        })
                        .forEach(li => ul.appendChild(li)
                    );
                };
            </script>

        </html>
    </xsl:template>
</xsl:stylesheet>
